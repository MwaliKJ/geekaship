/**
 * Created by James on 2015-10-08.
 */
import java.io.File;

public class Directory_tree {
    public static void main(String[] args) {
        walkin(new File("Newfolder")); //Replace this with a suitable directory
    }


    public static void walkin(File dir) {
        String pattern = ".txt";

        File listFile[] = dir.listFiles();
        if (listFile != null) {
            for (int i=0; i<listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    walkin(listFile[i]);
                } else {
                    if (listFile[i].getName().endsWith(pattern)) {
                        System.out.println(listFile[i].getPath());
                    }
                }
            }
        }
    }
}
