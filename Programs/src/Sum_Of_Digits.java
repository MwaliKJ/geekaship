/**
 * Created by James on 2015-09-22.
 */

public class Sum_Of_Digits {
    public static void main(String[] args) {

        int num = 10532;
        int sum = 0;
        while (num > 0) {
            sum = sum + num % 10;
            num = num / 10;
        }
        System.out.println(sum);
    }
}
