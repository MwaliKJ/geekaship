/**
 * Created by Lillian on 2015/10/03.
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
public class KnuthMorrisPrattStringSearch {
    private int[] failure;
    public KnuthMorrisPrattStringSearch(String text, String pat) {

        failure = new int[pat.length()];
        fail(pat);
        /** find match **/
        int position = posMatch(text, pat);
        if (position == -1)
            System.out.println("\nSorry No Match Found");
        else
            System.out.println("\nMatch found at index " + position);
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Knuth Morris Pratt String Search\n");
        System.out.println("\nPlease enter the text\n");
        String text = br.readLine();
        System.out.println("\nPlease enter the pattern\n");
        String pattern = br.readLine();
        KnuthMorrisPrattStringSearch MorrisPratt = new KnuthMorrisPrattStringSearch(text, pattern);
    }

    /**
     * Method to check if the pattern matches with the string or not
     **/
    private void fail(String pat) {
        int count = pat.length();
        failure[0] = -1;
        for (int value = 1; value < count; value++) {
            int index = failure[value - 1];
            while ((pat.charAt(value) != pat.charAt(index + 1)) && index >= 0)
                index = failure[index];
            if (pat.charAt(value) == pat.charAt(index + 1))
                failure[value] = index + 1;
            else
                failure[value] = -1;
        }
    }

    /**
     * Method to find match for a pattern
     **/
    private int posMatch(String text, String pat) {
        int match = 0, pattern = 0;
        int lens = text.length();
        int lenp = pat.length();
        while (match < lens && pattern < lenp) {
            if (text.charAt(match) == pat.charAt(pattern)) {
                match++;
                pattern++;
            } else if (pattern == 0)
                match++;
            else
                pattern = failure[pattern - 1] + 1;
        }
        return ((pattern == lenp) ? (match - lenp) : -1);
    }

}
