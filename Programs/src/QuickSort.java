/**
 * Created by James on 2015-09-28.
 */
public class QuickSort {
    private int array[];
    private int length;

    public void sort(int[] inputArray) {

        if (inputArray == null || inputArray.length == 0) {
            return;
        }
        this.array = inputArray;
        length = inputArray.length;
        quickSort(0, length - 1);
    }

    private void quickSort(int minIndex, int maxIndex) {

        int i = minIndex;
        int j = maxIndex;
        //calculation of the pivot number
        int pivot = array[minIndex+(maxIndex-minIndex)/2];

        while (i <= j) {

            while (array[i] < pivot) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                swap(i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
        // call quickSort() method recursively
        if (minIndex < j)
            quickSort(minIndex, j);
        if (i < maxIndex)
            quickSort(i, maxIndex);
    }

    private void swap(int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static void main(String a[]){

        QuickSort sorter = new QuickSort();
        int[] elements = {24,2,45,20,56,75,2,56,99,53,12};
        sorter.sort(elements);
        for(int i:elements){
            System.out.print(i);
            System.out.print(" ");
        }
    }

}
