/**
 * Created by James on 2015-10-08.
 */
public class Twelve_times_table {
    public static void main(String[] args) {


        int a = 0;
        int b = 0;

        for(a = 1; a <=12;a++){
            for (b = 1; b <=12;b++){
                int sum = a * b;
                System.out.println(a + " + " + b + " = " + sum);
            }
            System.out.println("");

        }
    }
}
