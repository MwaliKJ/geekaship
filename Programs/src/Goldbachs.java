/**
 * Created by James on 2015-10-05.
 */
public class Goldbachs {


    public static void main(String[] args) {

        // to check predefined input value

        int check_value = 28;
        int i = 0;
        if(check_value%2 != 0){
            System.out.println("Invalid input");
        }
        else{
            for(i=1; i<check_value/2; i=i+2){
                if(isPrime(i) && isPrime(check_value-i)){
                    System.out.println("Goldbach of "+ check_value +" = "+ i + " && "+(check_value-i));
                }
            }
        }
    }

    public static boolean isPrime(int n){
        int i;
        boolean flag=false;
        if(n%2 == 0){
            return false;
        }
        for(i=3; i<Math.sqrt(n); i=i+2){
            if(n%i ==0){
                return false;
            }
        }

        return true;
    }


}
