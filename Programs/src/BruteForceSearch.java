/**
 * Created by James on 2015-09-29.
 */

import java.util.*;

public class BruteForceSearch {
    private char[] input;
    private char[] pattern;
    private int lengthOfText;
    private int pattern2;

    public void setString(String text , String pattern) {
        this.input = text.toCharArray();
        this.pattern = pattern.toCharArray();
        this.lengthOfText = text.length();
        this.pattern2= pattern.length();
    }

    public int search() {
        for (int outerloop = 0; outerloop < lengthOfText - pattern2; outerloop++) {
            int innerloop = 0;
            while (innerloop < pattern2 && input[outerloop + innerloop] == pattern[innerloop]) {
                innerloop++;
            }
            if (innerloop == pattern2) return outerloop;
        }
        return -1;
    }

    public static void main(String[] args) {
        BruteForceSearch obj = new BruteForceSearch();
        System.out.println("Enter your text to be searched :");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println("Enter your pattern to use in order to search: ");
        String pattern = scanner.nextLine();

        obj.setString(text, pattern);
        int pos = obj.search();
        System.out.println("The text '" + pattern + "' is first found after the " + pos + " position.");


    }

}
