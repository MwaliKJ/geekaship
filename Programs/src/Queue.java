/**
 * Created by James on 2015-10-10.
 */
import java.util.PriorityQueue;

class Student implements Comparable <Student> {

    private int id;
    private String name;
    private int grade;
    private double score;


    public Student(int i, String n, int gr, double s) {
        id = i;
        name = n;
        grade = gr;
        score = s;

    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public double getScore() {
        return score;
    }

    public boolean equals(Student other) {
        return this.getID() == other.getID();
    }

    public int compareTo(Student other) {

        if (this.equals(other)) {
            return 0;
        } else if (getID() > other.getID())
            return 1;
        else
            return -1;
    }

    public String toString() {

        return "ID :: " + getID() + " Name :: " + getName() + " Grade :: " + getGrade() + " Score :: " + getScore() + "}";
    }
}

public class Queue {

    public static void main(String[] args) {

        Student James = new Student(1,"James",15,4.0);
        Student Bob = new Student(2,"Bob",9,3.5);
        Student Mwali = new Student(3,"Mwali",10,3.5);



        PriorityQueue<Student> pQueue = new PriorityQueue<Student>();

        pQueue.add(James);
        pQueue.add(Bob);
        pQueue.add(Mwali);

        int count = 1;

        while(pQueue.isEmpty()){
            System.out.println(pQueue);
            System.out.println("Dequeued " + count + " --> " + pQueue.remove());
            count++;
        }

        System.out.println();



    }
}
