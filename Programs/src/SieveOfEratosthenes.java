import java.util.Scanner;

/**
 * Created by James on 2015-09-30.
 */

public class SieveOfEratosthenes {


    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);
        System.out.println("Display prime numbers up until this number: ");
        int num = scan.nextInt();

        int[] numbers = new int[num + 1];

        int count = 2;
        for (int i = 2; i <= num; i++) {
            numbers[i] = count;
            count++;
        }

        for(int i = 2; (i * i) <= num; i++ ){
            for (int j = (i * i); j <= num; j = j + i) {
                numbers[j] = 0;
            }
        }
        for (int i = 2; i <= num; i++) {
            if (numbers[i] != 0) {
                System.out.println("Prime Number: " + numbers[i]);
            }
        }
    }
}
