/**
 * Created by James on 2015-09-21.
 */
public class Max {

    public static void main(String[] args) {

        int number[] = {11,4,8,91,5,12,14,116,25,40,32,21,36,39,8,90,12};
        int highest = number[0];
        int length = number.length;

        for(int i = 0; i < length;i++ ) {

            if(number[i] > highest){
                highest = number[i];
            }
        }
        System.out.print("The highest value in the Array is: " + highest);
    }
}
