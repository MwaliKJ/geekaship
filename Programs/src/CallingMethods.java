/**
 * Created by James on 2015-10-08.
 */
import java.util.Scanner;
public class CallingMethods {

    private int firstValue;
    private int secondValue;
    private String name;
    private static int prints = 4;

    //Calling a method that requires no arguments
    CallingMethods() {

    }
    //Arguments are passed by reference
    public String getName() {
        String name = "James";
        System.out.println("Name is : " + name);
        return name;
    }
    //Arguments passed by value
    public void Surname(){
        String surname = "Mwali";
        System.out.println("Surname is : " + surname);
    }

    /**
     * Obtaining the return value of a method
     * Calling a protected method
     */
    protected int Total(int firstValue, int secondValue,int sum)
    {
        this.firstValue = firstValue;
        this.secondValue = secondValue;

        sum = firstValue + secondValue;

        return sum;
    }

    protected int Total(int number2) {
        return this.secondValue;
    }
    //calling public methods

    public void PrintOutputs() {

        System.out.println(prints);
    }

    public static void main(String args[]) {


        CallingMethods methods = new CallingMethods();

        int results = methods.Total(12,13,0);

        int value=methods.Total(56);
        methods.PrintOutputs();
        int value1 = methods.Total(67);
        System.out.println(results);
        System.out.println(value1);
        System.out.println(value);
        methods.getName();
        methods.Surname();



    }
}

