/**
 * Created by James on 2015-09-18.
 */
public class sum_of_numbers {

    public static void Print_sums(){

        int [] numbers = { 1, 2, 3, 4, 5 ,6, 7, 8, 9 , 10 };
        int sum = 0;
        for(int i = 0; i < numbers.length; i++){
            sum += numbers[i];
        }

        System.out.println("The sum is: " + sum);

    }


    public static void main(String[] args) {

        Print_sums();

    }
}
