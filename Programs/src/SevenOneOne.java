import java.text.DecimalFormat;

/**
 * Created by Lillian on 2015/10/08.
 */
public class SevenOneOne {
    public static void main(String[] args) {
        //calculate price for each item and display the results
        for (double four = 1.00; four < 7.12; four += 0.01) {
            four = roundTwoDecimals(four);
            for (double three = 1.00; three < 7.12; three += 0.01) {
                three = roundTwoDecimals(three);
                for (double two = 1.00; two < 7.12; two += 0.01) {
                    two = roundTwoDecimals(two);
                    for (double one = 0.00; one < 7.12; one += 0.01) {
                        one = roundTwoDecimals(one);
                        if (roundTwoDecimals(one + two + three + four) == 7.11
                                && roundTwoDecimals(one * two * three
                                * four) == 7.11) {
                            System.out.println("Prices for the items are as follows\n" + "\nItem 1: " + one + "\nItem 2: " + two + "\nItem 3: "
                                    + three + "\nItem 4: " + four);
                            System.exit(0);
                        }
                    }

                }
            }
        }
    }

private static double roundTwoDecimals(double d) {
        DecimalFormat twoDFormat = new DecimalFormat("#.##");
        return Double.valueOf(twoDFormat.format(d));
        }
}
