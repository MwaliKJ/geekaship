import javax.swing.*;
import javax.swing.text.StringContent;

/**
 * Created by Carmin on 9/21/2015.
 */
public class Commas {
    public static void main(String args[])
    {
        String OpenBrace ="{";
        String ClosedBrace ="}";
        String textInput = JOptionPane.showInputDialog(null,"Enter the text :");
        if (textInput.isEmpty())
        {
            System.out.println(OpenBrace+ClosedBrace);
        }
        else{
            System.out.print(OpenBrace + textInput.replace(",", " and "));
        }

        System.out.println(ClosedBrace);

    }

}
