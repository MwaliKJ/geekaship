/**
 * Created by James on 2015-09-30.
 */

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedWriter;



public class TakingNotes {


    public static void main(String[] args) {

        try {

//Incomplete - need to append a new line
            String fileName = "NOTES.txt";

            String data = "This will be appended at the end of the  file";
            File file = new File(fileName);


            //  file.createFile();

            if (!file.exists()) {
                file.createNewFile();
                System.out.println("File named " + fileName + " is created");
            } else {

                System.out.println("File named " + fileName + " already exists -- Appending...");
            }

            FileWriter fileWriter = new FileWriter(file.getName(),true);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(data);
            //    bufferedWriter.write(data2);
            bufferedWriter.close();


            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}



