/**
 * Created by James on 2015-09-21.
 */
public class AllOdd {

    public static void main(String[] args) {

        for (int i = 1; i < 100;i++) {

            if(i%2!=0) {
                System.out.println(i);
            } else if(i!=99){
                System.out.println(",");
            }
        }
    }
}
