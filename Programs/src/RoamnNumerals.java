import javax.swing.*;

/**
 * Created by James on 2015-10-08.
 */
public class RoamnNumerals {


        private static int decodeSingle(char letter) {
            switch(letter) {
                case 'M': return 1000;
                case 'D': return 500;
                case 'C': return 100;
                case 'L': return 50;
                case 'X': return 10;
                case 'V': return 5;
                case 'I': return 1;
                default: return 0;
            }
        }
        public static int decode(String roman) {
            int result = 0;
            String uRoman = roman.toUpperCase();
            for(int i = 0;i < uRoman.length() - 1;i++) {//loop over all but the last character
                //if this character has a lower value than the next character
                if (decodeSingle(uRoman.charAt(i)) < decodeSingle(uRoman.charAt(i+1))) {
                    //subtractive form
                    result -= decodeSingle(uRoman.charAt(i));
                } else {
                    //additive form
                    result += decodeSingle(uRoman.charAt(i));
                }
            }
            //decode the last character, which is always added
            result += decodeSingle(uRoman.charAt(uRoman.length()-1));
            return result;
        }


        public static void main(String[] args) {
            String inputstring = JOptionPane.showInputDialog(null, "Enter the roman numerals either in additive form or subtractive form") ;
            String[] romanvalue = inputstring.split(",");
            String sum = "";
            String finalvalue = "";

            for (int i = 0; i < romanvalue.length; i++) {
                try {
                /*Convert the numbers in string to int*/
                    finalvalue =romanvalue[i];
                } catch (Exception error) {

                }
                sum += finalvalue;
            }
            System.out.println(decode(finalvalue));

        }
    }


