import java.awt.*;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by James on 2015-10-09.
 */

public class RPS {
    public static void main(String args[]) {

        String choice, choice1;
        int num = 0;
        Random r = new Random();
        num = r.nextInt(3);

        int PlayerLoses, PlayerWins;
        int CompLoses, CompWins;
        int Ties = 0;

        PlayerLoses = 0;
        PlayerWins = 0;
        CompLoses = 0;
        CompWins = 0;

        int NumPlays = 5;


        System.out.println("*Welcome to Rock-Paper-Scissor game, Press Q or q to quit*");
        System.out.println(" ");
        System.out.println("Choose: R for Rock, P for Paper or S for Scissors: ");
        Scanner userChoice = new Scanner(System.in);
        choice1 = userChoice.next();
        choice = choice1.toLowerCase();


      //  do{

            switch (num) {

                case 0:
                    String num1 = "r";
                    System.out.println("Computer chose Rock");
                    if (choice.matches(num1)) {
                        System.out.println("Its a tie!");
                        Ties++;

                    } else if (choice.matches("p")) {
                        System.out.println("You win!");
                        PlayerWins++;

                    } else if (choice.matches("s")) {
                        System.out.println("You lose!");
                        PlayerLoses++;
                    } else {
                        System.out.println();
                        System.out.println("ERROR: Please choose Rock, Paper or Scissors");
                    }
                    break;
                case 1:
                    String num2 = "p";
                    System.out.println("Computer chose Paper");
                    if (choice.matches(num2)) {
                        System.out.println("Its a tie!");
                        Ties++;

                    } else if (choice.matches("s")) {
                        System.out.println("You win!");
                        PlayerWins++;

                    } else if (choice.matches("r")) {
                        System.out.println("You lose!");
                        PlayerLoses++;
                    } else {
                        System.out.println();
                        System.out.println("ERROR: Please choose Rock, Paper or Scissors");
                    }
                    break;
                case 2:
                    String num3 = "s";
                    System.out.println("Computer chose Scissors");
                    if (choice.matches(num3)) {
                        System.out.println("Its a tie!");
                        Ties++;

                    } else if (choice.matches("r")) {
                        System.out.println("You win!");
                        PlayerWins++;

                    } else if (choice.matches("p")) {
                        System.out.println("You lose!");
                        PlayerLoses++;
                    } else {
                        System.out.println();
                        System.out.println("ERROR: Please choose Rock, Paper or Scissors");
                    }
                    break;


            }


       // }while(choice != "Q" || choice1 != "q");

        System.out.println("------------------------------------------------------------");
        System.out.println("Stats**********");
        System.out.println("Player Won " + PlayerWins + " time(s)");
        System.out.println("Player Lost " + PlayerLoses + " time(s)");
        System.out.println("Computer Won " + CompWins + " time(s)");
        System.out.println("Computer Lost " + CompLoses + " time(s)");

        System.out.println("Both Player and Computer tied: " + Ties + " time(s)");


    }
}
