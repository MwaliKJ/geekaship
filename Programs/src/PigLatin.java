/**
 * Created by James on 2015-09-29.
 */
import java.util.Scanner;
import java.util.*;
import java.io.*;

import java.io.IOException;

public class PigLatin {

    public static void main(String[] args) {

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter any word: ");
            String input = br.readLine();

            input = input.toLowerCase(); //converting the word into Uppercase
            int len = input.length();
            int pos = -1;
            char ch;

            for (int i = 0; i < len; i++) {
                ch = input.charAt(i);
                if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                    pos = i; //storing the index of the first vowel
                    break;
                }
            }

            if (pos != -1) //printing piglatin only if vowel exists
            {
                String a = input.substring(pos);
                String b = input.substring(0, pos);
                String pig = a + "-" + b + "ay";
                System.out.println("The Piglatin of the word = " + pig);
            } else
                System.out.println("No vowel, hence piglatin not possible");


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}


