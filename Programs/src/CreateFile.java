import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfAppearance;
import com.lowagie.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;

/**
 * Created by James on 9/17/2015.
 */
public class CreateFile {

    public  CreateFile(){

    }

    public CreateFile(String fileName, Object[] arrayOfArrays, String alignmentType) throws Exception{


        Document filetoWrite = new Document();

        File file = new File(fileName);

        OutputStream outST=new FileOutputStream(file.getAbsoluteFile());




        PdfWriter.getInstance(filetoWrite, outST);
        file.mkdir();



        filetoWrite.open();



        String[] line1 =(String[])arrayOfArrays[0];
        String[] line2=(String[])arrayOfArrays[1];
        String[] line3=(String[])arrayOfArrays[2];
        String[] line4=(String[])arrayOfArrays[3];
        String[] line5=(String[])arrayOfArrays[4];





        Table tableLeft = new Table(5, line1.length);
        Table tableRight = new Table(5, line1.length);
        Table tableCenter = new Table(5, line1.length);

        tableLeft.setSpacing(1);
        tableRight.setSpacing(1);
        tableCenter.setSpacing(1);



        //start adding infor for left
        TableFields(tableLeft, line1, "left");
        TableFields(tableLeft, line2, "left");
        TableFields(tableLeft, line3, "left");
        TableFields(tableLeft, line4, "left");
        TableFields(tableLeft, line5, "left");

        //end adding infor for left

        //start adding infor for Right
        TableFields(tableRight, line1, "right");
        TableFields(tableRight, line2, "right");
        TableFields(tableRight, line3, "right");
        TableFields(tableRight, line4, "right");
        TableFields(tableRight, line5, "right");

        //end adding infor for Right

        //start adding infor for Center
        TableFields(tableCenter, line1, "center");
        TableFields(tableCenter, line2, "center");
        TableFields(tableCenter, line3, "center");
        TableFields(tableCenter, line4, "center");
        TableFields(tableCenter, line5, "center");

        //end adding infor for Center

        filetoWrite.add(tableLeft);
        filetoWrite.add(tableRight);
        filetoWrite.add(tableCenter);

        filetoWrite.close();

    }

    public void TableFields(Table table, String[] data, String alignmentType){

        for (int i = 0; i <data.length ; i++) {

            Cell cell = new Cell(data[i]);


            if(alignmentType.equalsIgnoreCase("right")){

                cell.setHorizontalAlignment(PdfAppearance.ALIGN_RIGHT);

            }
            else if(alignmentType.equalsIgnoreCase("center")){

                cell.setHorizontalAlignment(PdfAppearance.ALIGN_CENTER);

            }

            table.addCell(cell);

        }


    }
}
