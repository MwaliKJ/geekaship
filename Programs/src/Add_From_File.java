import java.io.*;

/**
 * Created by James on 2015-09-22.
 */

import java.util.*;
import java.io.IOException;


public class Add_From_File {

    public static void main(String[] args){

        try{

            System.out.println(calcTotal(CalculateLines()));
        }
        catch (IOException e){

        }

    }

    public static int CalculateLines() throws IOException{
        int lines=0;

        File file = new File("Sample.txt");
        FileReader frd = new FileReader(file);
        BufferedReader brd = new BufferedReader(frd);

        while(brd.readLine()!=null){
            lines++;
        }
        return lines;
    }

    public static String calcTotal(int lines)throws  IOException{
        int sum=0;
        File file = new File("Sample.txt");
        FileReader frd = new FileReader(file);
        String answer="";
        BufferedReader brd = new BufferedReader(frd);
        for (int i = 0; i <lines ; i++) {
            int number=Integer.parseInt(brd.readLine());

            answer=answer+number+" + ";

            sum+=number;


            //  System.out.print();
        }

        answer=answer.substring(0,answer.length()-3);
        answer = answer+" = "+sum;

        return  answer;
    }



}
