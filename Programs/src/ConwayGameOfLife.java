/**
 * Created by Lillian on 2015/10/07.
 */

import java.awt.*;
import java.awt.event.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import javax.swing.*;

public class ConwayGameOfLife {
    public static void main(String[] args) {
        String[] dish = {
                "_#_",
                "_#_",
                "_#_",};
        int gens = 3;
        for (int i = 0; i < gens; i++) {
            System.out.println("Generation " + i + ":");
            print(dish);
            dish = life(dish);
        }
    }

    public static String[] life(String[] dish) {
        String[] newGen = new String[dish.length];
        for (int row = 0; row < dish.length; row++) {
            newGen[row] = "";
            for (int i = 0; i < dish[row].length(); i++) {
                String above = "";
                String same = "";
                String below = "";
                if (i == 0) {
                    above = (row == 0) ? null : dish[row - 1].substring(i,
                            i + 2);
                    same = dish[row].substring(i + 1, i + 2);
                    //no one below if on the bottom row
                    //else grab the neighbors from below
                    below = (row == dish.length - 1) ? null : dish[row + 1]
                            .substring(i, i + 2);
                } else if (i == dish[row].length() - 1) {
                    //no one above if on the top row
                    //otherwise grab the neighbors from above
                    above = (row == 0) ? null : dish[row - 1].substring(i - 1,
                            i + 1);
                    same = dish[row].substring(i - 1, i);
                    //no one below if on the bottom row
                    //else grab the neighbors from below
                    below = (row == dish.length - 1) ? null : dish[row + 1]
                            .substring(i - 1, i + 1);
                } else {//anywhere else
                    //no one above if on the top row
                    //else grab the neighbors from above
                    above = (row == 0) ? null : dish[row - 1].substring(i - 1,
                            i + 2);
                    same = dish[row].substring(i - 1, i)
                            + dish[row].substring(i + 1, i + 2);
                    //no one below if on the bottom row
                    //else grab the neighbors from below
                    below = (row == dish.length - 1) ? null : dish[row + 1]
                            .substring(i - 1, i + 2);
                }
                int neighbors = getNeighbors(above, same, below);
                if (neighbors < 2 || neighbors > 3) {
                    newGen[row] += "_";
                } else if (neighbors == 3) {
                    newGen[row] += "#";
                } else {
                    newGen[row] += dish[row].charAt(i);
                }
            }
        }
        return newGen;
    }

    public static int getNeighbors(String above, String same, String below) {
        int answer = 0;
        //increment if someone is there
        if (above != null) {
            for (char check : above.toCharArray()) {
                if (check == '#') answer++;
            }
        }
        for (char check : same.toCharArray()) {
            if (check == '#') answer++;
        }
        if (below != null) {
            for (char check : below.toCharArray()) {
                if (check == '#') answer++;
            }
        }
        return answer;
    }

    public static void print(String[] dish) {
        for (String s : dish) {
            System.out.println(s);
        }

    }
}