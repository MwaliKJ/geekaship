import java.util.ArrayList;

/**
 * Created by James on 2015-09-21.
 */
public class Reverse {

    public static String reverseString(String value){

        char[] array = new char[value.length()];

        for(int i = 0; i < array.length;i++){

            array[i] = value.charAt(value.length() - i - 1);
        }

        return new String(array);
    }






    public static void main(String[] args) {

        String org = "James";

        int number[] = {1,2,3,4,5,6,7,8,9,10};



        //reverseArray(number);

        int revIndex = number.length-1;
        for (int i = 0; i <number.length ; i++) {
            System.out.print(number[revIndex]+", ");
            revIndex--;
        }

        String reversed = reverseString(org);

        System.out.println(reversed);
    }
}
