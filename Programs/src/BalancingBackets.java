/**
 * Created by James on 2015-10-02.
 */
import java.util.Stack;
import java.util.Scanner;

public class BalancingBackets {



    public static void main(String[] args)
    {
        Scanner stdin = new Scanner(System.in);
        String expression;

        System.out.println("Please type a string containing brackets");
        System.out.println("[ ]. I'll check whether");
        System.out.println("the brackets are balanced.");

        do
        {
            System.out.print("Your string: ");
            expression = stdin.nextLine( );
            if (isBalanced(expression))
                System.out.println("OK.");
            else
                System.out.println("NOT OK.");
        }
        while (query(stdin, "Another string?"));


    }
    public static boolean query(Scanner input, String prompt)
    {
        String answer;

        System.out.print(prompt + " [Y or N]: ");
        answer = input.nextLine( ).toUpperCase( );
        while (!answer.startsWith("Y") && !answer.startsWith("N"))
        {
            System.out.print("Invalid response. Please type Y or N: ");
            answer = input.nextLine( ).toUpperCase( );
        }

        return answer.startsWith("Y");
    }

    public static boolean isBalanced(String expression)
    {
        // Meaningful names for characters
        final char LEFT_SQUARE  = '[';
        final char RIGHT_SQUARE = ']';

        Stack<Character> store = new Stack<Character>( ); // Stores parens
        int i;                              // An index into the string
        boolean failed = false;             // Change to true for a NOT OK

        for (i = 0; !failed && (i < expression.length( )); i++)
        {
            switch (expression.charAt(i))
            {

                case LEFT_SQUARE:
                    store.push(expression.charAt(i));
                    break;

                case RIGHT_SQUARE:
                    if (store.isEmpty( ) || (store.pop( ) != LEFT_SQUARE))
                        failed = true;
                    break;
            }
        }

        return (store.isEmpty( ) && !failed);
    }

}

